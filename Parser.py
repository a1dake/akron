import sys
import requests
import cfscrape
import urllib3
from bs4 import BeautifulSoup
from urllib3 import exceptions
from datetime import datetime, timedelta

sys.path.append("/home/manage_report")


from Send_report.mywrapper import magicDB

class Parser:
    def __init__(self, parser_name: str):
        self.session = cfscrape.create_scraper(sess=requests.Session())
        self.result_data: dict = {'name': parser_name,
                                  'data': []}

    @magicDB
    def run(self):
        content: list = self.get_data()
        self.result_data['data'] = content
        print(content)
        return self.result_data

    def get_ads(self):
        date_format = '%d.%m.%Y %H:%M'
        today = datetime.now()
        yesterday = today - timedelta(days=1)
        tomorrow = today + timedelta(days=1)

        from_date: str = yesterday.strftime(date_format)
        to_date: str = tomorrow.strftime(date_format)
        print('date from: {}, to: {}'.format(from_date, to_date))

        ads = []
        urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
        session = requests.Session()
        url = 'https://etp.acron.ru/searchServlet?query=%7B%22types%22%3A%5B%22BUYING%22%5D%7D&filter=%7B%22state%22%3A%5B%22GD%22%5D%7D&sort=%7B%22placementDate%22%3Afalse%7D&limit=%7B%22min%22%3A0%2C%22max%22%3A100%7D'
        session.headers.update({
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36 Edg/107.0.1418.56'
        })
        session.get('https://www.acron.ru/', verify=False)

        json_data = 'query=%7B%22types%22%3A%5B%22BUYING%22%5D%7D&filter=%7B%22state%22%3A%5B%22GD%22%5D%7D&sort=%7B%22placementDate%22%3Afalse%7D&limit=%7B%22min%22%3A0%2C%22max%22%3A100%7D'
        response = session.get(url, json=json_data, verify=False)
        data = response.json()

        for item in data.get('list'):
            date = datetime.strptime(item.get('gdStartDate'), '%d.%m.%Y %H:%M')
            if yesterday < date:
                ads.append(item.get('offerLink'))
        return ads

    def get_data(self):
        ads = self.get_ads()
        data = []
        for link in ads:
            content = self.get_content(link)
            data.append(content)
        return data

    def get_content(self, link):
        content = {}
        try:
            session = requests.Session()
            session.headers.update({
                'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
                'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.143 Safari/537.36'
            })
            response = session.get(link)
            tree = BeautifulSoup(response.text, 'lxml')

            item_data = {
                'type': 2,
                'title': '',
                'purchaseNumber': '',
                'fz': 'Коммерческие',
                'purchaseType': '',
                'url': '',
                'lots': [],
                'attachments': [],
                'procedureInfo': {
                    'endDate': '',
                    'scoringDate': '',
                    'biddingDate': ''
                },

                'customer': {
                    'fullName': ''
                }
            }
            item_data['title'] = str(self.get_title(tree))
            item_data['url'] = str(link)

            item_data['purchaseType'] = str(self.get_type(tree))
            item_data['purchaseNumber'] = str(self.get_number(tree))

            item_data['customer']['fullName'] = str(self.get_customer(tree))

            item_data['procedureInfo']['endDate'] = self.get_end_date(tree)
            item_data['procedureInfo']['scoringDate'] = self.get_scor_date(tree)
            item_data['procedureInfo']['biddingDate'] = self.get_bid_date(tree)

            item_data['lots'] = self.get_lots(tree)

            item_data['attachments'] = self.get_attach(tree)

            content = item_data
        except Exception as e:
            print(e)
            print("Страница с ошибкой ", link)

        return content

    def get_title(self, tree):
        try:
            data = tree.select_one('table[class*="styled order_info_table"]').select('tr')[1].select('td')[1].get_text()
        except Exception as e:
            print(e)
            data = ''
        return data

    def get_type(self, tree):
        try:
            data = tree.select_one('table[class*="styled order_info_table"]').select('tr')[3].select('td')[1].get_text()
        except:
            data = ''
        return data

    def get_number(self, tree):
        try:
            data = tree.select_one('span[class*="identifier"]').get_text()
        except:
            data = ''
        return data

    def get_customer(self, tree):
        try:
            data = tree.select_one('table[class*="styled order_info_table"]').select('tr')[2].select('td')[1].get_text()
        except:
            data = ''
        return data

    def get_end_date(self, tree):
        try:
            ru_date = ' '.join((tree.select_one('table[id*="tablewrapper"]').select('tr')[3].select('td')[1].get_text()).strip().split('\xa0'))
            data = self.prepare_item_date(ru_date)
            formated_date = self.formate_date(data)
        except:
            formated_date = None
        return formated_date

    def get_scor_date(self, tree):
        try:
            ru_date = ' '.join((tree.select_one('table[id*="tablewrapper"]').select('tr')[4].select('td')[1].get_text()).strip().split('\xa0'))
            data = self.prepare_item_date(ru_date)
            formated_date = self.formate_date(data)
        except:
            formated_date = None
        return formated_date

    def get_bid_date(self, tree):
        try:
            ru_date = ' '.join((tree.select_one('table[id*="tablewrapper"]').select('tr')[5].select('td')[1].get_text()).strip().split('\xa0'))
            data = self.prepare_item_date(ru_date)
            formated_date = self.formate_date(data)
        except:
            formated_date = None
        return formated_date

    def get_lots(self, tree):
        try:
            data = []
            lots = {
                'region': '',
                'address': '',
                'price': '',
                'lotItems': []

            }
            lot_div = tree.select_one('table[id*="tablewrapper_0"]').select('tr')

            for lot in lot_div[1:]:
                names = {'code': str(lot.select('td')[0].get_text().strip()),
                         'name': str(lot.select('td')[1].get_text().strip())}
                lots['lotItems'].append(names)

            lots['price'] = str(tree.select_one('table[id*="tablewrapper_0"]').select('tr')[1].select('td')[2].get_text().strip())

            data.append(lots)
        except:
            data = []
        return data

    def get_attach(self, tree):
        data = []
        try:
            doc_div = tree.select_one('table[id*="tablewrapper_1"]').select('tr')

            for doc in doc_div[1:]:
                docs = {'docDescription': str(doc.select('td')[0].select_one('a').get_text().strip()),
                        'url': 'https://etp.acron.ru/' + str(doc.select('td')[0].select_one('a')['href'])}

                data.append(docs)
        except:
            data = []
        return data

    def prepare_item_date(self, date):
        months = {
            'янв': '01',
            'фев': '02',
            'мар': '03',
            'апр': '04',
            'май': '05',
            'июн': '06',
            'июл': '07',
            'авг': '08',
            'сен': '09',
            'окт': '10',
            'ноя': '11',
            'дек': '12',
        }
        day = date.split(' ')[0]
        month = date.split(' ')[1]
        year = date.split(' ')[2]
        month = month.replace(month, months[month]).strip()
        date = '.'.join([day, month, year])
        return date

    def formate_date(self, old_date):
        date_object = datetime.strptime(old_date, '%d.%m.%Y')
        formatted_date = date_object.strftime('%H.%M.%S %d.%m.%Y')
        return formatted_date
